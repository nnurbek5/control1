"use strict";

var t = require('node-fetch');

var text2 = "",
    text3 = "",
    c = 0;

var deleteScript = function deleteScript(s) {
  var SCRIPT_REGEX = /()<[^<]*\)*<\/script>/gi;

  while (SCRIPT_REGEX.test(s)) {
    s = s.replace(SCRIPT_REGEX, "");
  }

  return s;
};

t('https://ru.wikipedia.org/wiki/TypeScript').then(function (res) {
  return res.text();
}).then(function (body) {
  var dataWithoutScripts = deleteScript(body);
  var dataWithoutHtmlTags = dataWithoutScripts.replace(/<[^>]+>/g, '');
  var dataWithoutLinebreaks = dataWithoutHtmlTags.replace(/[\r\n]+/gm, '');
  var dataWithoutMultipleSpaces = dataWithoutLinebreaks.replace(/  +/g, ' ');
  var dataWithoutTabs = dataWithoutMultipleSpaces.replace(/\t+/g, ' ');
  var dataWithoutAnd = dataWithoutTabs.replace(/[0-9]\&\#[0-9][0-9]\;/g, '');
  var dataWithoutAnd1 = dataWithoutAnd.replace(/\&\#[0-9][0-9]\;/g, '');
  var dataWithoutAnd2 = dataWithoutAnd1.replace(/\&\#[0-9][0-9][0-9]\;/g, '');

  while (c < dataWithoutAnd2.length) {
    text2 = text2 + dataWithoutAnd2[c];
    c++;
  }

  body = text2;
  body = body.split(".");
  c = 0;

  while (c < body.length) {
    text3 = text3 + body[c];
    text3 = text3.trim() + "\n";
    c++;
  }

  console.log("Кол-во строк - " + body.length);
});